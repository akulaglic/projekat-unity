﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vrijeme : MonoBehaviour {

    private Text text;
    float currTime = 0f;
    public GameObject svjetlo;
    public GameObject pozadina;
    
   
    public AudioSource source;

    // Use this for initialization
    void Start () {
       
        text = GetComponent<Text>();
        svjetlo.SetActive(false);
        
    }


    private void Update()
    {
       
        if (Manager.isActive)
        {
            pozadina.SetActive(true);
            currTime += Time.deltaTime;
            UpdateLevelTimer(currTime);
            
        }
    }

    public void UpdateLevelTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }
        if (minutes == 5 && seconds == 0)
        {
            source.Play();
            svjetlo.SetActive(true);
            
        }
        Manager.trenutnoVrijeme = minutes.ToString("00") + ":" + seconds.ToString("00");

        text.text = minutes.ToString("00") + ":" + seconds.ToString("00");
        
    }
}
