﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prozor : MonoBehaviour {

    bool otvoren = false;

    public AudioSource zatvoriSound;
    private Renderer gameObject;
    public  AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        gameObject = GetComponent<Renderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (transform.GetComponent<Animation>().IsPlaying("OtvoriProzor") && Manager.isActive && 
                Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
            {
                
                source.Play();
                zatvoriSound.Stop();
            }
                
            else if (!transform.GetComponent<Animation>().IsPlaying("OtvoriProzor") && source.isPlaying && Manager.isActive &&
                 Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 3f)
            {
                
                zatvoriSound.Play();
                source.Pause();
            }
                

            if (!otvoren)
            {
                if (!transform.GetComponent<Animation>().IsPlaying("ZatvoriProzor") && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 3f)
                {

                    transform.GetComponent<Animation>().CrossFade("OtvoriProzor", 0.5f, PlayMode.StopAll);
                    source.Play();
                    zatvoriSound.Stop();

                    otvoren = true;
                }


            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("OtvoriProzor") && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {

                    transform.GetComponent<Animation>().Play("ZatvoriProzor");
                    zatvoriSound.Play();
                    source.Pause();
                    otvoren = false;
                }

            }
        }

    }

    
}