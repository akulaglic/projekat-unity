﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roletna : MonoBehaviour {

    bool podignuta = false;
    public AudioClip RoletnaSound;
    private Renderer gameObject;
    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        gameObject = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1) && Manager.isActive)
        {
            

            if (!podignuta)
            {
                if (!transform.GetComponent<Animation>().IsPlaying("RoletnaDole") && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 3f)
                {

                    transform.GetComponent<Animation>().CrossFade("RoletnaGore", 0.5f, PlayMode.StopAll);
                    source.PlayOneShot(RoletnaSound, 1F);

                    podignuta = true;
                }


            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("RoletnaGore") && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 5f)
                {

                    transform.GetComponent<Animation>().Play("RoletnaDole");
                    source.PlayOneShot(RoletnaSound, 1F);
                    podignuta = false;
                }

            }
        }

    }
}