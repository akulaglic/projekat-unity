﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fotelja : MonoBehaviour {

    bool povucena = false;
    public AudioClip FoteljaSound;
    private Renderer gameObject;

    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        gameObject = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (!povucena)
            {
           
                if (!transform.GetComponent<Animation>().IsPlaying("FoteljaNazad") && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 3f)
                {
                    transform.GetComponent<Animation>().CrossFade("FoteljaNaprijed", 0.5f, PlayMode.StopAll);
                    source.PlayOneShot(FoteljaSound, 1F);

                    povucena = true;
                }

            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("FoteljaNaprijed") && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 3f)
                {
                    transform.GetComponent<Animation>().Play("FoteljaNazad");
                    source.PlayOneShot(FoteljaSound, 1F);
                    povucena = false;
                }

            }
        }

    }
}