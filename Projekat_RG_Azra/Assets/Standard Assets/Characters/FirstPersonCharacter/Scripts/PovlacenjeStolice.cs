﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PovlacenjeStolice : MonoBehaviour {

    bool povucena = false;
    public AudioClip StolicaSound;

    public GameObject gameObject;
    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (!povucena)
            {
                //print("nije povucena i" + Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position).ToString());
                if (!transform.GetComponent<Animation>().IsPlaying("PovlacenjeStoliceNaprijed") && gameObject.GetComponent<Renderer>().isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                    
                    transform.GetComponent<Animation>().CrossFade("PovlacenjeStoliceNazad", 0.5f, PlayMode.StopAll);
                    source.PlayOneShot(StolicaSound, 1F);

                    povucena = true;
                }
               
            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("PovlacenjeStoliceNazad") && gameObject.GetComponent<Renderer>().isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                   
                    transform.GetComponent<Animation>().Play("PovlacenjeStoliceNaprijed");
                    source.PlayOneShot(StolicaSound, 1F);
                    povucena = false;
                }
               
            }
        }

    }
}