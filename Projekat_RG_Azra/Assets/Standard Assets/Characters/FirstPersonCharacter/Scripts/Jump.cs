﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {
    public AudioSource jump;
    public AudioSource land;
   
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space) && !jump.isPlaying && Manager.isActive) jump.Play();
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (Manager.isActive)
        {
            switch (collision.gameObject.name)
            {
                case "pod":
                    land.Play();
                    break;
                
            }
        }
       
        
    }
}
