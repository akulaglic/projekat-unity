﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Svjetlo : MonoBehaviour {

    bool upaljeno = false;
    public GameObject sijalica;
    public GameObject sijalicaPocetak;
    private Renderer gameObject;
    public AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
         sijalica.SetActive(false);
         sijalicaPocetak.SetActive(true);
        gameObject = GetComponent<Renderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            
           
            if (!upaljeno)
            {
                if ( Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 1.5f && gameObject.isVisible)
                {
                    
                    transform.eulerAngles = new Vector3(0, 0, -0.45f);
                    sijalica.SetActive(true);
                    sijalicaPocetak.SetActive(false);
                    upaljeno = true;
                    source.Play();

                }


            }
            else
            {
                
                if ( Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 1.5f && gameObject.isVisible)
                {
                    
                    transform.eulerAngles = new Vector3(0, 0, 9.172f);
                    sijalica.SetActive(false);
                    sijalicaPocetak.SetActive(true);
                    source.Play();
                    upaljeno = false;
                }
            }
           
        }

    }
}