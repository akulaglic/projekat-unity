﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour {

    private CharacterController cc;
    public AudioSource source;

    // Use this for initialization
    void Start () {
        cc = GetComponent<CharacterController>();
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if(cc.isGrounded == true && cc.velocity.magnitude>2f && source.isPlaying==true)
        {
            source.Play();
        }
	}
}
