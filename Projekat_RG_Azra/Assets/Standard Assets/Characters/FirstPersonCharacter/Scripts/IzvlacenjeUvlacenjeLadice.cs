﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IzvlacenjeUvlacenjeLadice : MonoBehaviour {

    bool izvucena = false;
    public AudioClip LadicaSound;
    private Renderer gameObject;

    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
        gameObject = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (!izvucena)
            {
                if (!transform.GetComponent<Animation>().IsPlaying("ZatvaranjeLadice")
                    && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                    //print("izvucena");
                    transform.GetComponent<Animation>().CrossFade("OtvaranjeLadice", 0.5f, PlayMode.StopAll);
                    source.PlayOneShot(LadicaSound, 1F);

                    izvucena = true;
                }


            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("OtvaranjeLadice")
                    && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                    //print("uvucena");
                    transform.GetComponent<Animation>().Play("ZatvaranjeLadice");
                    source.PlayOneShot(LadicaSound, 1F);
                    izvucena = false;
                }
            }
        }

    }
}
