﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crouch : MonoBehaviour
{
    public float walkSpeed = 7; // regular speed
    public float crchSpeed = 3; // crouching speed
    public float runSpeed = 20; // run speed

    private Transform tr;
    private float dist; // distance to ground

    // Use this for initialization
    void Start()
    {
        tr = transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float vScale = 1.0f;
        float speed = walkSpeed;

       
        if (Input.GetKey("c"))
        { // press C to crouch
            vScale = 0.5f;
            speed = crchSpeed; // slow down when crouching
        }
        float ultScale = tr.localScale.y; // crouch/stand up smoothly 

        Vector3 tmpScale = tr.localScale;
        Vector3 tmpPosition = tr.position;

        tmpScale.y = Mathf.Lerp(tr.localScale.y, vScale, 5 * Time.deltaTime);
        tr.localScale = tmpScale;

        tmpPosition.y += tr.localScale.y - ultScale; // fix vertical position        
        tr.position = tmpPosition;
    }
}
