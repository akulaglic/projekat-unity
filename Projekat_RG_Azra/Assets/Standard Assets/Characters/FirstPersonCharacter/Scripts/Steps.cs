﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Steps : MonoBehaviour {


    public AudioSource step;
    public GameObject menu;
    public GameObject finalni;
    public GameObject lazni;
    public GameObject lupa;
    public GameObject kljuc;
    public GameObject kljuc1;
    public GameObject ogledalo;
    public GameObject ogledalo1;
    Text[] text;
    public GameObject gameObject;

    public AudioClip PobjedaSound;
    public AudioClip LazniSound;

    private AudioSource source;
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical")) && Manager.isActive)
            step.Play();
        else if (!Input.GetButton("Horizontal") && !Input.GetButton("Vertical") && step.isPlaying && Manager.isActive)
            step.Pause(); // or Pause()

        if (Input.GetKeyDown("m") && Manager.isActive)
        {
            menu.SetActive(true);

            Manager.isActive = false;
            step.Pause();
        }
        else if (Input.GetKeyDown("m") && !Manager.isActive)
        {

            menu.SetActive(false);
            Manager.isActive = true;

        }
        else if (Vector3.Distance(lupa.transform.position, GameObject.Find("Igrac").transform.position) < 2f &&
                Input.GetMouseButtonDown(1) && Manager.isActive && gameObject.GetComponent<Renderer>().isVisible)
        {


            finalni.SetActive(true);
            source.PlayOneShot(PobjedaSound, 1F);
            Manager.isActive = false;

            text = finalni.GetComponentsInChildren<Text>();
            text[1].text = Manager.trenutnoVrijeme;


            step.Pause();
           
        }
        else if ((Vector3.Distance(kljuc.transform.position, GameObject.Find("Igrac").transform.position) < 2f
            || Vector3.Distance(kljuc1.transform.position, GameObject.Find("Igrac").transform.position) < 2f
            || Vector3.Distance(ogledalo.transform.position, GameObject.Find("Igrac").transform.position) < 4f
            || Vector3.Distance(ogledalo1.transform.position, GameObject.Find("Igrac").transform.position) < 2f) &&
               Input.GetMouseButtonDown(1) && Manager.isActive )
        {


            lazni.SetActive(true);
             source.PlayOneShot(LazniSound, 1F);
            Manager.isActive = false;
            step.Pause();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && lazni.active)
        {
            lazni.SetActive(false);
            Manager.isActive = true;
        }       
    }
    
}