﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Komande : MonoBehaviour {


    public GameObject igra;
    
    public void QuitGame()
    {
        Application.Quit();
    }

    public void PocniIgru()
    {
        igra.SetActive(false);
        Manager.isActive = true;
    }
}
