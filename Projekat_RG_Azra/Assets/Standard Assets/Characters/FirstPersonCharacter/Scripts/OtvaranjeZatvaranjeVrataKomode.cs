﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtvaranjeZatvaranjeVrataKomode : MonoBehaviour {

    bool otvorena = false;
    public AudioClip LadicaSound;
    public AudioClip LadicaSoundZatvori;
    private Renderer gameObject;

    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        gameObject = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (!otvorena)
            {
                if (!transform.GetComponent<Animation>().IsPlaying("ZatvaranjeVrataDesno") && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                   
                    transform.GetComponent<Animation>().Play("OtvaranjeVrataDesno");
                    source.PlayOneShot(LadicaSound, 1F);

                    otvorena = true;
                }
               

            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("OtvaranjeVrataDesno") && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                   
                    transform.GetComponent<Animation>().Play("ZatvaranjeVrataDesno");
                    source.PlayOneShot(LadicaSoundZatvori, 1F);
                    otvorena = false;
                }
                
            }
        }
    }
}
