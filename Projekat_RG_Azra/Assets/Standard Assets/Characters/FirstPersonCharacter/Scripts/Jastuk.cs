﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jastuk : MonoBehaviour
{
    bool pomjeren = false;
    public AudioClip JastukSound;
    private Renderer gameObject;
    private AudioSource source;
    private float vollowLarge = .2f;
    private float vollowRange = .5f;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        gameObject = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Manager.isActive)
        {
            if (!pomjeren)
            {
                if (!transform.GetComponent<Animation>().IsPlaying("VratiJastuk") && gameObject.isVisible
                    && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                   
                    transform.GetComponent<Animation>().CrossFade("PomijeranjeJastuka", 0.5f, PlayMode.StopAll);
                    source.PlayOneShot(JastukSound, 1F);

                    pomjeren = true;
                }
                

            }
            else
            {
                if (!transform.GetComponent<Animation>().IsPlaying("PomijeranjeJastuka") && gameObject.isVisible
                     && Vector3.Distance(transform.position, GameObject.Find("Igrac").transform.position) < 2f)
                {
                   
                    transform.GetComponent<Animation>().Play("VratiJastuk");
                    source.PlayOneShot(JastukSound, 1F);
                    pomjeren = false;
                }
               
            }
        }

    }
}